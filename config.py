from os import environ, path


__basedir = path.abspath(path.dirname(__file__))

# Main
DEBUG = True

# Secrets
SECRET_KEY = "secret"

# SQLAlchemy
SQLALCHEMY_DATABASE_URI = "sqlite:///" + path.join(__basedir, 'comentap.sqlite')
SQLALCHEMY_TRACK_MODIFICATIONS = False
