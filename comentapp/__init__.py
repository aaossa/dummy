from .app import app
from .models import db
from . import views


db.create_all()
