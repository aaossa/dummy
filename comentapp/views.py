from flask import flash, jsonify, redirect, render_template, request, url_for
from json import dumps
from .app import app
from .forms import CommentForm
from .models import db, Comment


@app.route("/")
def index():
    """Display a form to create a comment."""
    comment_form = CommentForm()
    return render_template(
        'index.html',
        form=comment_form,
    )


@app.route("/comment/new", methods=["POST"])
def create_comment():
    """Create a new comment and store it in the database."""
    comment_form = CommentForm()
    # Validate form
    if not comment_form.validate_on_submit():
        # flash_errors(comment_form)
        return redirect(url_for('index'))
    # Retrieve validated data
    body = request.form['body']
    ip = request.headers.get('X-Forwarded-For', request.remote_addr)
    # ip =  request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
    # ip = request.remote_addr
    # Create Comment object
    new_comment = Comment(body, ip)
    # Save new_comment in db
    db.session.add(new_comment)
    db.session.commit()
    # Feedback
    # flash('Comment successfully added')
    return redirect(url_for('show_comments'))


@app.route("/comments", methods=["GET"])
def show_comments():
    """Show existing comments."""
    # Retrieve and serialize comments
    all_comments = Comment.query.all()
    results = list(map(
        lambda x: x.to_dict(),
        all_comments,
    ))
    return jsonify(results)


# @app.route("/users/<id>", methods=["GET"])
# def get_user(id):
#     # Retrieve and serialize user
#     user = User.query.get(id)
#     return jsonify(user.to_json())
