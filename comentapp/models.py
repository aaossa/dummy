from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from .app import app

db = SQLAlchemy(app)


class Comment(db.Model):
    """An anonymous comment created in the website.

    :param int id: Unique id for this comment
    :param str body: Body of the comment
    :param str ip: IP of comment creator
    """
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String, nullable=False)
    ip = db.Column(db.String, nullable=False)
    # Store creation date
    created_at = db.Column(db.DateTime, default=datetime.utcnow)

    def __init__(self, body, ip):
        self.body = body
        self.ip = ip

    def __repr__(self):
        """Return a string representation of a comment."""
        return "<Comment {}: {} ({}, {})>".format(
            self.id,
            self.body,
            self.ip,
            self.created_at,
        )

    def to_dict(self):
        """Return a dict representation of the object"""
        return {
            "id": self.id,
            "body": self.body,
            "ip": self.ip,
            "created_at": self.created_at,
        }
